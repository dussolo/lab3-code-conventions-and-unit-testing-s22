package com.hw.db.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.*;

class ThreadControllerTest {
    static final LocalDateTime THREAD_CREATED = LocalDateTime.of(2022, 2, 21, 17, 30);

    private Thread idThread;
    private Thread slugThread;
    private Thread threadModifier;
    private List<Post> posts;
    private List<Vote> votes;
    private List<User> users;

    @BeforeEach
    void setup() {
        idThread = createIdThread();
        slugThread = createSlugThread();
        threadModifier = createThreadModifier();
        users = createUsers();
        posts = new ArrayList<>();
        votes = new ArrayList<>();
    }

    private MockedStatic<ThreadDAO> mockThreadDAO() {
        List<Thread> threads = Arrays.asList(slugThread, idThread);

        MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class);
        threadDAO
                .when(() -> ThreadDAO.getThreadById(any(Integer.class)))
                .then(invocationOnMock -> {
                    Integer id = invocationOnMock.getArgument(0);

                    return threads.stream()
                            .filter(thread -> Objects.equals(thread.getId(), id)).findFirst()
                            .orElseThrow(() -> new EmptyResultDataAccessException(1));
                });
        threadDAO
                .when(() -> ThreadDAO.getThreadBySlug(any(String.class)))
                .then(invocationOnMock -> {
                    String slug = invocationOnMock.getArgument(0);

                    return threads.stream()
                            .filter(thread -> Objects.equals(thread.getSlug(), slug)).findFirst()
                            .orElseThrow(() -> new EmptyResultDataAccessException(1));
                });
        threadDAO
                .when(() -> ThreadDAO.change(any(Thread.class), any(Thread.class)))
                .then(invocationOnMock -> {
                    Thread original = invocationOnMock.getArgument(0);
                    Thread update = invocationOnMock.getArgument(1);

                    assignThread(original, update);

                    return null;
                });
        threadDAO
                .when(() -> ThreadDAO.getPosts(anyInt(), any(), any(), any(), any()))
                .then(invocationOnMock -> {
                    Integer threadId = invocationOnMock.getArgument(0);

                    return posts.stream()
                            .filter(post -> Objects.equals(post.getThread(), threadId))
                            .collect(Collectors.toList());
                });
        threadDAO
                .when(() -> ThreadDAO.createVote(any(Thread.class), any(Vote.class)))
                .then(invocationOnMock -> {
                    Thread thread = invocationOnMock.getArgument(0);
                    Vote newVote = invocationOnMock.getArgument(1);

                    if (votes.stream()
                            .anyMatch(vote -> (
                                    Objects.equals(vote.getNickname(), newVote.getNickname())
                                            && Objects.equals(vote.getTid(), newVote.getTid())
                            ))) {
                        throw new DuplicateKeyException("dup vote");
                    }


                    newVote.setTid(thread.getId());
                    thread.setVotes(thread.getVotes() + newVote.getVoice());

                    votes.add(newVote);

                    return null;
                });
        threadDAO
                .when(() -> ThreadDAO.change(any(Vote.class), anyInt()))
                .then(invocationOnMock -> {
                    Vote newVote = invocationOnMock.getArgument(0);

                    Vote oldVote = votes.stream()
                            .filter(vote -> (
                                    Objects.equals(vote.getNickname(), newVote.getNickname())
                                            && Objects.equals(vote.getTid(), newVote.getTid())
                            ))
                            .findAny()
                            .orElseThrow(() -> new EmptyResultDataAccessException(1));

                    Thread thread = threads.stream()
                            .filter(th -> Objects.equals(th.getId(), newVote.getTid()))
                            .findAny()
                            .orElseThrow(() -> new EmptyResultDataAccessException(1));

                    thread.setVotes(thread.getVotes() - oldVote.getVoice() + newVote.getVoice());

                    newVote.setTid(thread.getId());

                    votes.set(votes.indexOf(oldVote), newVote);

                    return thread.getVotes();
                });

        threadDAO
                .when(() -> ThreadDAO.createPosts(any(Thread.class), anyList(), anyList()))
                .then(invocationOnMock -> {
                    Thread thread = invocationOnMock.getArgument(0);
                    List<Post> newPosts = invocationOnMock.getArgument(1);
                    List<User> users = invocationOnMock.getArgument(2);

                    if (newPosts.size() != users.size()) {
                        throw new IndexOutOfBoundsException();
                    }

                    for (int i = 0; i < newPosts.size(); i++) {
                        Post post = newPosts.get(i);
                        post.setAuthor(users.get(i).getNickname());

                        if (post.getParent() != null) throw new EmptyResultDataAccessException(1);
                    }

                    linkPosts(newPosts, thread);

                    posts.addAll(newPosts);

                    return null;
                });

        return threadDAO;
    }

    private MockedStatic<UserDAO> mockUserDAO() {
        MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class);

        userDAO
                .when(() -> UserDAO.Info(any(String.class)))
                .then(invocationOnMock -> {
                    String nick = invocationOnMock.getArgument(0);

                    return users.stream().filter(user -> Objects.equals(user.getNickname(), nick))
                            .findAny()
                            .orElseThrow(() -> new EmptyResultDataAccessException(1));
                });

        return userDAO;
    }

    private static List<User> createUsers() {
        List<User> users = new ArrayList<>();


        users.add(new User("gman", "gman@gov.gov", "G-Man", "Nobody knows who is he"));
        users.add(new User("gfreeman", "gfreeman@bmrf.com", "Gordon Freeman", "Everybody knows him"));
        users.add(new User("wbreen", "wbreen@bmrf.com", "Wallace Breen", "Administrator"));

        return users;
    }

    private static Thread createIdThread() {
        Thread idThread = new Thread("foobar", Timestamp.valueOf(THREAD_CREATED), "forum1", "Lorem ipsum un dolor sit amet.", "id-thread", "Thread to find by id", 1);
        idThread.setId(11);
        return idThread;
    }

    private static Thread createSlugThread() {
        Thread slugThread = new Thread("Wallace Breen", Timestamp.valueOf(THREAD_CREATED), "forum1", //
                "Don't you worry, you'll never see a resonance cascade!", "slug-thread", //
                "Resonance cascade won't happen", 1);

        slugThread.setId(10);

        return slugThread;
    }

    private static Thread createThreadModifier() {
        Thread modifier = new Thread();

        modifier.setAuthor("Gordon Freeman");
        modifier.setTitle("Resonance cascade happened!");
        modifier.setMessage("Hide everybody, resonance cascade just happened!");

        return modifier;
    }

    private static List<Post> createSamplePosts() {
        List<Post> posts = new ArrayList<>();

        posts.add(new Post("wbreen",
                null,
                null,
                "Probability of resonance cascade to happen is <1%",
                null,
                null,
                false
        ));

        posts.add(new Post("gfreeman",
                null,
                null,
                "Where did you get this number from, you liar?",
                null,
                null,
                false
        ));

        posts.add(new Post("wbreen",
                null,
                null,
                "It's confidential. You fired for rudeness.",
                null,
                null,
                true
        ));

        return posts;
    }

    private static void linkPosts(List<Post> posts, Thread thread) {
        posts.forEach(post -> {
            post.setId(post.hashCode());
            post.setCreated(Timestamp.from(Instant.now()));
            post.setThread(thread.getId());
            post.setForum(thread.getForum());
        });
    }

//    private static void createPostUnknownUser();

    private static void assignThread(Thread original, Thread update) {

        if (update.getCreated() != null) {
            original.setCreated(update.getCreated());
        }

        if (update.getSlug() != null) {
            original.setSlug(update.getSlug());
        }

        if (update.getAuthor() != null) {
            original.setAuthor(update.getAuthor());
        }
        if (update.getMessage() != null) {
            original.setMessage(update.getMessage());
        }
        if (update.getTitle() != null) {
            original.setTitle(update.getTitle());
        }
    }

    private static Vote createSampleUpVote() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue("{\"nickname\": \"gman\",\"voice\": 1}", Vote.class);
    }

    private static Vote createSampleDownVote() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue("{\"nickname\": \"gman\",\"voice\": -1}", Vote.class);

    }

    private static Vote createSampleNoUserVote() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue("{\"nickname\": \"foobar\",\"voice\": 1}", Vote.class);

    }


    // region ThreadController#checkIdOrSlug
    @Test
    void checkIdOrSlugFoundById() {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            ThreadController controller = new ThreadController();
            Thread foundThread = controller.checkIdOrSlug(idThread.getId().toString());

            Assertions.assertSame(idThread, foundThread);
        }
    }

    @Test
    void checkIdOrSlugFoundBySlug() {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            ThreadController controller = new ThreadController();
            Thread foundThread = controller.checkIdOrSlug(slugThread.getSlug());

            Assertions.assertSame(slugThread, foundThread);
        }
    }

    @Test
    void checkIdOrSlugNotFoundById() {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            ThreadController controller = new ThreadController();

            assertThatThrownBy(() -> controller.checkIdOrSlug("294929"))
                    .isInstanceOf(DataAccessException.class);
        }
    }

    @Test
    void checkIdOrSlugNotFoundBySlug() {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            ThreadController controller = new ThreadController();

            assertThatThrownBy(() -> controller.checkIdOrSlug("random-slug"))
                    .isInstanceOf(DataAccessException.class);
        }
    }
    // endregion

    // region ThreadController#createPost
    @Test
    void createPostAddsNewPosts() {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            try (MockedStatic<UserDAO> ignored1 = mockUserDAO()) {
                ThreadController controller = new ThreadController();

                List<Post> newPosts = createSamplePosts();

                ResponseEntity<?> response = controller.createPost(slugThread.getSlug(), newPosts);

                assertThat(posts).containsAll(newPosts);

                assertThat(response.getBody())
                        .isInstanceOf(List.class)
                        .asList()
                        .hasSameElementsAs(newPosts);
            }
        }
    }

    @Test
    void createPostParentNotFound() {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            try (MockedStatic<UserDAO> ignored1 = mockUserDAO()) {
                ThreadController controller = new ThreadController();

                List<Post> newPosts = createSamplePosts();
                newPosts.get(1).setParent(-1);

                ResponseEntity<?> response = controller.createPost(slugThread.getSlug(), newPosts);

                assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
            }
        }
    }

    @Test
    void createPostThreadNotFound() {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            try (MockedStatic<UserDAO> ignored1 = mockUserDAO()) {
                ThreadController controller = new ThreadController();

                List<Post> newPosts = createSamplePosts();

                ResponseEntity<?> response = controller.createPost("random-slug", newPosts);

                assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
            }
        }
    }

    @Test
    void createPostUserNotFound() {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            try (MockedStatic<UserDAO> ignored1 = mockUserDAO()) {
                ThreadController controller = new ThreadController();

                List<Post> newPosts = createSamplePosts();
                newPosts.get(0).setAuthor("foobar");


                ResponseEntity<?> response = controller.createPost(slugThread.getSlug(), newPosts);

                assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
            }
        }
    }
    // endregion

    // region ThreadController#posts
    @Test
    void postsFound() {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            ThreadController controller = new ThreadController();

            posts = createSamplePosts();
            linkPosts(posts, slugThread);

            ResponseEntity<?> response = controller.posts(slugThread.getSlug(), null, null, null, null);

            assertThat(response.getBody()).asList().hasSameElementsAs(posts);
        }
    }

    @Test
    void postsNotFound() {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            ThreadController controller = new ThreadController();

            ResponseEntity<?> response = controller.posts("random-slug", null, null, null, null);

            Assertions.assertSame(response.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }
    // endregion

    // region ThreadController#change

    @Test
    void changeSucceeds() {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            ThreadController controller = new ThreadController();

            ResponseEntity<?> response = controller.change(slugThread.getSlug(), threadModifier);

            Thread expectedThread = createSlugThread();
            assignThread(expectedThread, threadModifier);

            assertThat(response.getBody()).isEqualToComparingFieldByField(expectedThread);
            assertThat(slugThread).isEqualToComparingFieldByField(expectedThread);
        }
    }

    @Test
    void changeFailsNotFound() {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            ThreadController controller = new ThreadController();

            ResponseEntity<?> response = controller.change("random-slug", threadModifier);

            Thread expectedThread = createSlugThread();
            assignThread(expectedThread, threadModifier);

            Assertions.assertSame(response.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }
    // endregion

    // region ThreadController#info
    @Test
    void infoFoundBySlug() {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            ThreadController controller = new ThreadController();

            ResponseEntity<?> response = controller.info(slugThread.getSlug());

            Assertions.assertSame(response.getBody(), slugThread);
        }
    }

    @Test
    void infoNotFoundBySlug() {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            ThreadController controller = new ThreadController();

            ResponseEntity<?> response = controller.info("random-thread");

            Assertions.assertSame(response.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }
    // endregion

    // region ThreadController#createVote
    @Test
    void createVoteAddsNewVote() throws IOException {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            try (MockedStatic<UserDAO> ignored1 = mockUserDAO()) {
                ThreadController controller = new ThreadController();

                Vote sampleVote = createSampleUpVote();

                Integer votesBefore = slugThread.getVotes();

                controller.createVote(slugThread.getSlug(), sampleVote);

                assertThat(votes.stream().anyMatch(vote -> (
                        Objects.equals(vote.getTid(), slugThread.getId())
                                && Objects.equals(vote.getNickname(), sampleVote.getNickname())
                                && Objects.equals(vote.getVoice(), sampleVote.getVoice())
                ))).isTrue();

                assertThat(slugThread.getVotes())
                        .isEqualTo(votesBefore + sampleVote.getVoice());
            }
        }
    }

    @Test
    void createVoteModifiesVote() throws IOException {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            try (MockedStatic<UserDAO> ignored1 = mockUserDAO()) {
                ThreadController controller = new ThreadController();
                Vote existentVote = createSampleUpVote();

                Integer votesBefore = slugThread.getVotes();

                ThreadDAO.createVote(slugThread, createSampleUpVote());

                Vote sampleVote = createSampleDownVote();

                controller.createVote(slugThread.getSlug(), sampleVote);

                assertThat(votes.stream().anyMatch(vote -> (
                        Objects.equals(vote.getTid(), slugThread.getId())
                                && Objects.equals(vote.getNickname(), sampleVote.getNickname())
                                && Objects.equals(vote.getVoice(), sampleVote.getVoice())
                ))).isTrue();
                assertThat(votes.stream().noneMatch(vote -> (
                        Objects.equals(vote.getTid(), slugThread.getId())
                                && Objects.equals(vote.getNickname(), existentVote.getNickname())
                                && Objects.equals(vote.getVoice(), existentVote.getVoice())
                ))).isTrue();

                assertThat(slugThread.getVotes())
                        .isEqualTo(votesBefore + sampleVote.getVoice());
            }
        }
    }

    @Test
    void createVoteThreadNotFound() throws IOException {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            try (MockedStatic<UserDAO> ignored1 = mockUserDAO()) {
                ThreadController controller = new ThreadController();

                Vote sampleVote = createSampleDownVote();

                ResponseEntity<?> response = controller.createVote("random-slug", sampleVote);

                assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

                assertThat(votes).isEmpty();
            }
        }
    }

    @Test
    void createVoteUserNotFound() throws IOException {
        try (MockedStatic<ThreadDAO> ignored = mockThreadDAO()) {
            try (MockedStatic<UserDAO> ignored1 = mockUserDAO()) {
                ThreadController controller = new ThreadController();

                Integer votesBefore = slugThread.getVotes();

                Vote sampleVote = createSampleNoUserVote();

                ResponseEntity<?> response = controller.createVote(slugThread.getSlug(), sampleVote);

                assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

                assertThat(votes).isEmpty();

                assertThat(slugThread.getVotes()).isEqualTo(votesBefore);
            }
        }
    }
    // endregion
}